package com.itau.fizzbuzz;

public class FizzBuzz {

	public static String fizzBuzz(int i) {
//		if (i % 3 == 0) {
//			return "fizz";
//		}
//		return null;
		if ((i % 3 ==0) && (i % 5 ==0))  {
			return "fizzbuzz";
		}
		
		if (i % 5 == 0) {
			return "buzz";
		}
		
		if ( i % 3 == 0) {
			return "fizz";
			
		}

		return Integer.toString(i);
		
		
	}
	
	public static String imprimeValores(int i) {
		String resultado = "";
		for (int y = 1; y <= i; y++) {
			resultado += FizzBuzz.fizzBuzz(y) + " "; 
		}
		
		return resultado.trim(); //"1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz";
	}

	
}
